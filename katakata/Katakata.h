#pragma once

#include <array>
#include "Key.h"

/// <summary>
/// アプリケーション
/// </summary>
class Katakata
{
public:
	/// <summary>
	/// コンストラクタ
	/// </summary>
	Katakata();

	/// <summary>
	/// デストラクタ
	/// </summary>
	~Katakata();

	/// <summary>
	/// 実行
	/// </summary>
	void Run();

protected:
	/// <summary>
	/// キー
	/// </summary>
	std::array<Key*, 48>* _keys;

	/// <summary>
	/// 動作
	/// </summary>
	bool Act();

	/// <summary>
	/// 描画
	/// </summary>
	void Draw();
};
