#include "Katakata.h"
#include "DxLib.h"
#include <atltrace.h>
#include <tchar.h>

/// <summary>
/// メイン
/// </summary>
/// <param name="hInstance">インスタンスハンドル</param>
/// <param name="hPrevInstance">未使用</param>
/// <param name="lpCmdLine">コマンドライン</param>
/// <param name="nCmdShow">初期ウィンドウ表示</param>
/// <returns>アプリケーション戻り値</returns>
int WINAPI WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow)
{
	int ret = 0;
	if ((ChangeWindowMode(TRUE) == DX_CHANGESCREEN_OK) && (DxLib_Init() == 0))
	{
		if (SetDrawScreen(DX_SCREEN_BACK) == 0)
		{
			Katakata *application = new Katakata();
			application->Run();
			delete application;
		}
		else
		{
			ret = -2;
		}
		DxLib_End();
	}
	else
	{
		ret = -1;
	}

	return ret;
}

Katakata::Katakata()
{
	Key::Initialize();
	this->_keys = new std::array<Key*, 48>{
		new Key(KEY_INPUT_1, _T("1"), 0, 0),
		new Key(KEY_INPUT_2, _T("2"), 1, 0),
		new Key(KEY_INPUT_3, _T("3"), 2, 0),
		new Key(KEY_INPUT_4, _T("4"), 3, 0),
		new Key(KEY_INPUT_5, _T("5"), 4, 0),
		new Key(KEY_INPUT_6, _T("6"), 5, 0),
		new Key(KEY_INPUT_7, _T("7"), 6, 0),
		new Key(KEY_INPUT_8, _T("8"), 7, 0),
		new Key(KEY_INPUT_9, _T("9"), 8, 0),
		new Key(KEY_INPUT_0, _T("0"), 9, 0),
		new Key(KEY_INPUT_MINUS, _T("-"), 10, 0),
		new Key(KEY_INPUT_PREVTRACK, _T("^"), 11, 0),
		new Key(KEY_INPUT_YEN, _T("\\"), 12, 0),
		new Key(KEY_INPUT_Q, _T("Q"), 0, 1),
		new Key(KEY_INPUT_W, _T("W"), 1, 1),
		new Key(KEY_INPUT_E, _T("E"), 2, 1),
		new Key(KEY_INPUT_R, _T("R"), 3, 1),
		new Key(KEY_INPUT_T, _T("T"), 4, 1),
		new Key(KEY_INPUT_Y, _T("Y"), 5, 1),
		new Key(KEY_INPUT_U, _T("U"), 6, 1),
		new Key(KEY_INPUT_I, _T("I"), 7, 1),
		new Key(KEY_INPUT_O, _T("O"), 8, 1),
		new Key(KEY_INPUT_P, _T("P"), 9, 1),
		new Key(KEY_INPUT_AT, _T("`"), 10, 1),
		new Key(KEY_INPUT_LBRACKET, _T("{"), 11, 1),
		new Key(KEY_INPUT_A, _T("A"), 0, 2),
		new Key(KEY_INPUT_S, _T("S"), 1, 2),
		new Key(KEY_INPUT_D, _T("D"), 2, 2),
		new Key(KEY_INPUT_F, _T("F"), 3, 2),
		new Key(KEY_INPUT_G, _T("G"), 4, 2),
		new Key(KEY_INPUT_H, _T("H"), 5, 2),
		new Key(KEY_INPUT_J, _T("J"), 6, 2),
		new Key(KEY_INPUT_K, _T("K"), 7, 2),
		new Key(KEY_INPUT_L, _T("L"), 8, 2),
		new Key(KEY_INPUT_SEMICOLON, _T("+"), 9, 2),
		new Key(KEY_INPUT_COLON, _T("*"), 10, 2),
		new Key(KEY_INPUT_RBRACKET, _T("}"), 11, 2),
		new Key(KEY_INPUT_Z, _T("Z"), 0, 3),
		new Key(KEY_INPUT_X, _T("X"), 1, 3),
		new Key(KEY_INPUT_C, _T("C"), 2, 3),
		new Key(KEY_INPUT_V, _T("V"), 3, 3),
		new Key(KEY_INPUT_B, _T("B"), 4, 3),
		new Key(KEY_INPUT_N, _T("N"), 5, 3),
		new Key(KEY_INPUT_M, _T("M"), 6, 3),
		new Key(KEY_INPUT_COMMA, _T("<"), 7, 3),
		new Key(KEY_INPUT_PERIOD, _T(">"), 8, 3),
		new Key(KEY_INPUT_SLASH, _T("?"), 9, 3),
		new Key(KEY_INPUT_BACKSLASH, _T("_"), 10, 3),
	};
}

Katakata::~Katakata()
{
	for (Key* key : *this->_keys)
	{
		delete key;
	}
	delete this->_keys;
}

void Katakata::Run()
{
	bool exit = false;
	bool state = false;
	while (!exit && (ProcessMessage() == 0))
	{
		if (ClearDrawScreen() == 0)
		{
			exit = this->Act();

			DxLib::ScreenFlip();
		}
	}
}

bool Katakata::Act()
{
	bool exit = false;

	char inputs[256];
	GetHitKeyStateAll(inputs);

	if (inputs[KEY_INPUT_ESCAPE] == 0)
	{
		for (Key* key : *this->_keys)
		{
			key->Act(inputs);
		}

		this->Draw();
	}
	else
	{
		exit = true;
	}

	return exit;
}

void Katakata::Draw()
{
	for (Key* key : *this->_keys)
	{
		key->Draw();
	}
}
