#include "Key.h"
#include "DxLib.h"

/// <summary>
/// ��
/// </summary>
const int WIDTH = 24;

int Key::RED = 0;
int Key::GREEN = 0;

void Key::Initialize()
{
	Key::RED = GetColor(255, 0, 0);
	Key::GREEN = GetColor(0, 255, 0);
}

Key::Key(unsigned int id, const TCHAR* key_name, unsigned char column_index, unsigned char row_index)
	: _id(id), _key_name(key_name), _column_index(column_index), _row_index(row_index), _pre_on(false), _flip(false)
{
	this->_x = (this->_column_index * 2 + this->_row_index) * WIDTH + 20;
	this->_x2 = this->_x + WIDTH;
	switch (this->_row_index)
	{
	case 0:
		this->_key_box_y = 251;
		this->_flow_y = 284;
		break;
	case 1:
		this->_key_box_y  = 259;
		this->_flow_y = 284;
		break;
	case 2:
		this->_key_box_y = 446;
		this->_flow_y = 365;
		break;
	case 3:
		this->_key_box_y = 454;
		this->_flow_y = 365;
		break;
	default:
		this->_key_box_y = 0;
		this->_flow_y = 0;
		break;
	}
	this->_key_box_y2 = this->_key_box_y + 24;
	this->_flow_y2 = this->_flow_y + 80;
}

void Key::Act(char inputs[])
{
	bool on = inputs[this->_id] == 1;
	if (on && !this->_pre_on)
	{
		this->_flip = !this->_flip;
	}
	this->_pre_on = on;
}

void Key::Draw()
{
	if (this->_pre_on)
	{
		DrawBox(this->_x, this->_key_box_y, this->_x2, this->_key_box_y2, Key::GREEN, TRUE);
	}
	else
	{
		DrawBox(this->_x, this->_key_box_y, this->_x2, this->_key_box_y2, Key::RED, FALSE);
	}
	DrawBox(this->_x, this->_flow_y, this->_x2, this->_flow_y2, Key::RED, FALSE);
	DrawString(this->_x + 8, this->_key_box_y + 4, this->_key_name, Key::RED);
}
