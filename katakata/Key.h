#pragma once

#include <tchar.h>

/// <summary>
/// キー
/// </summary>
class Key
{
public:
	/// <summary>
	/// 初期化
	/// </summary>
	static void Initialize();

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name="id">ID</param>
	/// <param name="key_name">キー名</param>
	/// <param name="column_index">列インデックス</param>
	/// <param name="row_index">行インデックス</param>
	Key(unsigned int id, const TCHAR * key_name, unsigned char column_index, unsigned char row_index);

	/// <summary>
	/// 動作
	/// </summary>
	/// <param name="inputs">入力</param>
	void Act(char inputs[]);

	/// <summary>
	/// 描画
	/// </summary>
	void Draw();

protected:
	/// <summary>
	/// 赤色
	/// </summary>
	static int RED;

	/// <summary>
	/// 緑色
	/// </summary>
	static int GREEN;

	/// <summary>
	/// ID
	/// </summary>
	unsigned int _id;

	/// <summary>
	/// 列インデックス
	/// </summary>
	unsigned char _column_index;

	/// <summary>
	/// 行インデックス
	/// </summary>
	unsigned char _row_index;

	/// <summary>
	/// キー名
	/// </summary>
	const TCHAR * _key_name;

	/// <summary>
	/// 左端
	/// </summary>
	int _x;

	/// <summary>
	/// 右端
	/// </summary>
	int _x2;

	/// <summary>
	/// キー箱上端
	/// </summary>
	int _key_box_y;

	/// <summary>
	/// キー箱下端
	/// </summary>
	int _key_box_y2;

	/// <summary>
	/// フロー上端
	/// </summary>
	int _flow_y;

	/// <summary>
	/// フロー下端
	/// </summary>
	int _flow_y2;

	/// <summary>
	/// 前回ON
	/// </summary>
	bool _pre_on;

	/// <summary>
	/// フリップ
	/// </summary>
	bool _flip;
};
